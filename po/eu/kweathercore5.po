# Translation for kweathercore5.po to Euskara/Basque (eu).
# Copyright (C) 2022 This file is copyright:
# This file is distributed under the same license as the kweathercore package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kweathercore\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-30 00:50+0000\n"
"PO-Revision-Date: 2022-10-19 05:25+0200\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.1\n"

#: kweathercore.cpp:119
#, kde-format
msgid "Immediate"
msgstr "Berehalakoa"

#: kweathercore.cpp:122
#, kde-format
msgid "Expected"
msgstr "Esperotakoa"

#: kweathercore.cpp:125
#, kde-format
msgid "Future"
msgstr "Etorkizunekoa"

#: kweathercore.cpp:128
#, kde-format
msgid "Past"
msgstr "Iraganekoa"

#: kweathercore.cpp:131 kweathercore.cpp:153 kweathercore.cpp:175
#: kweathercore_p.h:23
#, kde-format
msgid "Unknown"
msgstr "Ezezaguna"

#: kweathercore.cpp:141
#, kde-format
msgid "Extreme"
msgstr "Muturrekoa"

#: kweathercore.cpp:144
#, kde-format
msgid "Severe"
msgstr "Gogorra"

#: kweathercore.cpp:147
#, kde-format
msgid "Moderate"
msgstr "Ertaina"

#: kweathercore.cpp:150
#, kde-format
msgid "Minor"
msgstr "Txikia"

#: kweathercore.cpp:163
#, kde-format
msgid "Observed"
msgstr "Behatua"

#: kweathercore.cpp:166
#, kde-format
msgid "Likely"
msgstr "Seguruenik"

#: kweathercore.cpp:169
#, kde-format
msgid "Possible"
msgstr "Baliteke"

#: kweathercore.cpp:172
#, kde-format
msgid "Unlikely"
msgstr "Nekez"

#: kweathercore.cpp:223 kweathercore.cpp:224 kweathercore.cpp:225
msgid "Clear"
msgstr "Garbia"

#: kweathercore.cpp:226 kweathercore.cpp:227 kweathercore.cpp:228
msgid "Cloudy"
msgstr "Hodeitsu"

#: kweathercore.cpp:229
msgid "Partly Sunny"
msgstr "Neurri batean eguzkitsu"

#: kweathercore.cpp:230 kweathercore.cpp:231
msgid "Light Clouds"
msgstr "Hodei arinak"

#: kweathercore.cpp:232 kweathercore.cpp:233 kweathercore.cpp:234
msgid "Fog"
msgstr "Lainoa"

#: kweathercore.cpp:236 kweathercore.cpp:237 kweathercore.cpp:238
#: kweathercore.cpp:242 kweathercore.cpp:243 kweathercore.cpp:244
msgid "Heavy Rain"
msgstr "Euri zaparrada"

#: kweathercore.cpp:239 kweathercore.cpp:240 kweathercore.cpp:241
#: kweathercore.cpp:245 kweathercore.cpp:246 kweathercore.cpp:247
#: kweathercore.cpp:252 kweathercore.cpp:253 kweathercore.cpp:254
#: kweathercore.cpp:258 kweathercore.cpp:259 kweathercore.cpp:260
#: kweathercore.cpp:268 kweathercore.cpp:269 kweathercore.cpp:270
#: kweathercore.cpp:275 kweathercore.cpp:276 kweathercore.cpp:277
#: kweathercore.cpp:281 kweathercore.cpp:282 kweathercore.cpp:283
#: kweathercore.cpp:288 kweathercore.cpp:289 kweathercore.cpp:290
#: kweathercore.cpp:298 kweathercore.cpp:299 kweathercore.cpp:300
#: kweathercore.cpp:305 kweathercore.cpp:306 kweathercore.cpp:307
#: kweathercore.cpp:308 kweathercore.cpp:309 kweathercore.cpp:310
#: kweathercore.cpp:319 kweathercore.cpp:320 kweathercore.cpp:321
#: kweathercore.cpp:325 kweathercore.cpp:326 kweathercore.cpp:327
#: kweathercore.cpp:332 kweathercore.cpp:333 kweathercore.cpp:334
#: kweathercore.cpp:338 kweathercore.cpp:339 kweathercore.cpp:340
#: kweathercore.cpp:351 kweathercore.cpp:352 kweathercore.cpp:353
msgid "Storm"
msgstr "Ekaitza"

#: kweathercore.cpp:249 kweathercore.cpp:250 kweathercore.cpp:251
#: kweathercore.cpp:255 kweathercore.cpp:256 kweathercore.cpp:257
msgid "Heavy Sleet"
msgstr "Elurbusti zaparrada"

#: kweathercore.cpp:262 kweathercore.cpp:263 kweathercore.cpp:264
#: kweathercore.cpp:265 kweathercore.cpp:266 kweathercore.cpp:267
msgid "Heavy Snow"
msgstr "Elur zaparrada"

#: kweathercore.cpp:272 kweathercore.cpp:273 kweathercore.cpp:274
#: kweathercore.cpp:278 kweathercore.cpp:279 kweathercore.cpp:280
msgid "Light Rain"
msgstr "Euri arina"

#: kweathercore.cpp:285 kweathercore.cpp:286 kweathercore.cpp:287
#: kweathercore.cpp:291 kweathercore.cpp:292 kweathercore.cpp:293
msgid "Light Sleet"
msgstr "Elurbusti arina"

#: kweathercore.cpp:295 kweathercore.cpp:296 kweathercore.cpp:297
#: kweathercore.cpp:301 kweathercore.cpp:302 kweathercore.cpp:303
msgid "Light Snow"
msgstr "Elur arina"

#: kweathercore.cpp:312 kweathercore.cpp:313 kweathercore.cpp:314
msgid "Partly Cloudy"
msgstr "Neurri batean hodeitsu"

#: kweathercore.cpp:316 kweathercore.cpp:317 kweathercore.cpp:318
#: kweathercore.cpp:322 kweathercore.cpp:323 kweathercore.cpp:324
msgid "Rain"
msgstr "Euria"

#: kweathercore.cpp:329 kweathercore.cpp:330 kweathercore.cpp:331
#: kweathercore.cpp:335 kweathercore.cpp:336 kweathercore.cpp:337
msgid "Sleet"
msgstr "Elurbustia"

#: kweathercore.cpp:342 kweathercore.cpp:343 kweathercore.cpp:344
#: kweathercore.cpp:345 kweathercore.cpp:346 kweathercore.cpp:347
#: kweathercore.cpp:348 kweathercore.cpp:349 kweathercore.cpp:350
msgid "Snow"
msgstr "Elurra"
